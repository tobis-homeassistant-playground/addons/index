# Tobis Homeassistant Playground - Addons

Add-ons for Home Assistant written by Tobias Kuntzsch.
To add this repository:
1. Go to your Home Assistant Frontend
2. Open **Supervisor** > **Add-On Store**,
3. Click on the top right menu with the three dots.
4. Click on **Repositories**
5. Paste the URL of this GitHub Repository and click **Add**

You should now see the add-ons of this repository in your Home Assistant Add-On Store.

## Add-ons provided by this repository

- **[PCloud Backup](/pcloud-backup)** - Sync your Home Assistant backups to PCloud
- **[Gitlab Runner](/gitlab-runner)** - Setup an Bash Gitlab Runner to update your Config or other Files
- **[Filament Database](/filament-database)** - Filament database backend for ESPHome device